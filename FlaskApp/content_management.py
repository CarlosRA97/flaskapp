def Content():
    TOPIC_DICT = {"Basics":[["Introduction to Python","/introduction-to-python-programming/"],
                            ["Print functions and Strings","/python-tutorial-print-function-strings/"],
                            ["Math basics with Python 3","/math-basics-python-3-beginner-tutorial/"]],
                  "Web Dev":[["Make web pages with Flask","/web-pages-with-flask/"],
                            ["Advanced Django tutorial","/advanced-django/"]],
                  "Data Analysis":[[]]
                  }

    return TOPIC_DICT
