#!/usr/bin/bash

sudo apt-get install git
sudo apt-get install apache2 mysql-client mysql-server
sudo apt-get install libapache2-mod-wsgi
sudo a2enmod wsgi

# cd /var/www/
# mkdir FlaskApp
# cd FlaskApp
# mkdir FlaskApp
# cd FlaskApp/
# mkdir static
# mkdir templates
# touch __init__.py

######## Template for Flask .py file ##########

# from flask import Flask
#
# app = Flask(__name__)
#
# @app.route('/')
# def homepage():
#    return "Hi there, how ya doin?"
#
#
# if __name__ == "__main__":
#    app.run()

###############################################

sudo apt-get update && apt-get upgrade
sudo apt-get install python-pip
sudo pip install virtualenv
sudo pip install Flask

sudo touch /etc/apache2/sites-available/FlaskApp.conf

###### Apache Configuration for FlaskApp ######

# <VirtualHost *:80>
#                 ServerName yourdomain.com
#                 ServerAdmin youemail@email.com
#                 WSGIScriptAlias / /var/www/FlaskApp/flaskapp.wsgi
#                 <Directory /var/www/FlaskApp/FlaskApp/>
#                         Order allow,deny
#                         Allow from all
#                 </Directory>
#                 Alias /static /var/www/FlaskApp/FlaskApp/static
#                 <Directory /var/www/FlaskApp/FlaskApp/static/>
#                         Order allow,deny
#                         Allow from all
#                 </Directory>
#                 ErrorLog ${APACHE_LOG_DIR}/error.log
#                 LogLevel warn
#                 CustomLog ${APACHE_LOG_DIR}/access.log combined
# </VirtualHost>

####### WSGI Configuration for FlaskApp #######

# a2ensite FlaskApp
# service apache2 reload
# cd /var/www/FlaskApp
# touch flaskapp.wsgi


# #!/usr/bin/python
# import sys
# import logging
# logging.basicConfig(stream=sys.stderr)
# sys.path.insert(0,"/var/www/FlaskApp/")
#
# from FlaskApp import app as application
# application.secret_key = 'your secret key. If you share your website, do NOT share it with this key.'

# service apache2 restart

###############################################
